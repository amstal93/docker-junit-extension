/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.util.regex.Pattern;

import com.ongres.process.FluentProcess;
import org.jooq.lambda.Unchecked;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;

@DockerExtension(value = {
    @DockerContainer(
        alias = "postgres",
        extendedBy = DockerExtensionConfiguration.class,
        whenReuse = WhenReuse.ALWAYS,
        stopIfChanged = true)
    },
    client = DockerClientType.PODMAN)
@DisabledIfEnvironmentVariable(named = "DOCKER_EXTENSION_TESTS",
    matches = "^((?!(?:^| )PODMAN(?:$| )).)*$")
public class DockerExtensionPodmanIt extends AbstractDockerExtensionIt {

  @Override
  protected String execFailTestExceptionMessagePattern() {
    return Pattern.quote("Command podman ")
        + "((exec|run --ulimit host) [^ ]+ "
        + Pattern.quote("sh -c echo 1; false") + "|start -a [^ ]+)"
        + Pattern.quote(" exited with code 1");
  }

  @Test
  @Override
  public void ipTest(@ContainerParam("postgres") Container postgres) throws Exception {
    Assumptions.assumeTrue(Unchecked.booleanSupplier(() -> {
      try {
        return FluentProcess.start("id", "-u").get().equals("0");
      } catch (Exception ex) {
        return false;
      }
    }));
    super.ipTest(postgres);
  }

  @Test
  @Override
  public void externalPortTest(@ContainerParam("postgres") Container postgres) throws Exception {
    Assumptions.assumeTrue(Unchecked.booleanSupplier(() -> {
      try {
        return FluentProcess.start("id", "-u").get().equals("0");
      } catch (Exception ex) {
        return false;
      }
    }));
    super.externalPortTest(postgres);
  }

}
