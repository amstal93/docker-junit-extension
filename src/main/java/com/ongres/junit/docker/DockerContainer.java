/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specify a docker container configuration.
 * Can be used inside `@DockerExtension` or to annotate a class as an override.
 *
 * @since 1.0.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DockerContainer {

  /**
   * the docker image's name (required if {@code extendedBy} is not specified).
   */
  String image() default "";

  /**
   * the docker container's alias (optional).
   */
  String alias() default "";

  /**
   * the arguments to pass to the docker container.
   */
  String[] arguments() default {};

  /**
   * the environment variables to pass to the docker container.
   */
  Environment[] environments() default {};

  /**
   * the mounts to pass to the docker container.
   */
  Mount[] mounts() default {};

  /**
   * the optional ports to expose for the docker container.
   */
  Port[] ports() default {};

  /**
   * the message to wait for in the docker container log before starting tests.
   * @see WaitFor
   */
  WaitFor waitFor() default @WaitFor();

  /**
   * determine when the container should be reused.
   */
  WhenReuse whenReuse() default WhenReuse.ALWAYS;

  /**
   * stop the container with same name if configuration changed.
   */
  boolean stopIfChanged() default false;

  /**
   * the class annotated with configuration that override this one.
   */
  Class<?> extendedBy() default Void.class;

  /**
   * the docker instance can fail start up to the value specified.
   */
  int retry() default 3;
}
