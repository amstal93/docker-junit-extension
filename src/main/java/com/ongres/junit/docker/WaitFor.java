/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

/**
 * Asks docker to wait for something to appear in the container's log before running any test.
 *
 * @since 1.0.0
 */
public @interface WaitFor {

  public static final String[] NOTHING = new String[0];

  /**
   * The default timeout duration.
   */
  public static final int DEFAULT_TIMEOUT = 30_000;

  /**
   * the log text sequence to wait for.
   */
  String[] value() default {};

  /**
   * the time in milliseconds to wait for the log before giving up.
   */
  int timeout() default DEFAULT_TIMEOUT;
}
